/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author e1197
 */
@Entity
@Table(name = "Rastidesc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rastidesc.findAll", query = "SELECT r FROM Rastidesc r")
    , @NamedQuery(name = "Rastidesc.findById", query = "SELECT r FROM Rastidesc r WHERE r.id = :id")
    , @NamedQuery(name = "Rastidesc.findByDesc1", query = "SELECT r FROM Rastidesc r WHERE r.desc1 = :desc1")
    , @NamedQuery(name = "Rastidesc.findByLat", query = "SELECT r FROM Rastidesc r WHERE r.lat = :lat")
    , @NamedQuery(name = "Rastidesc.findByLon", query = "SELECT r FROM Rastidesc r WHERE r.lon = :lon")
    , @NamedQuery(name = "Rastidesc.findByName", query = "SELECT r FROM Rastidesc r WHERE r.name = :name")})
public class Rastidesc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "desc1")
    private String desc1;
    @Column(name = "lat")
    private String lat;
    @Column(name = "lon")
    private String lon;
    @Column(name = "name")
    private String name;

    public Rastidesc() {
    }

    public Rastidesc(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDesc1() {
        return desc1;
    }

    public void setDesc1(String desc1) {
        this.desc1 = desc1;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rastidesc)) {
            return false;
        }
        Rastidesc other = (Rastidesc) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "map.Rastidesc[ id=" + id + " ]";
    }
    
}
