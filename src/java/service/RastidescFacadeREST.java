/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import map.Rastidesc;

/**
 *
 * @author Antto Hautamäki
 * REST rajapinta
 */
@Stateless
@Path("map.rastidesc")
public class RastidescFacadeREST extends AbstractFacade<Rastidesc> {

    private EntityManager em = Persistence.createEntityManagerFactory("WebApplication4PU").createEntityManager();

    public RastidescFacadeREST() {
        super(Rastidesc.class);
    }

    @POST

    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public Response create(Rastidesc entity) {

        em.merge(entity);
        em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();

        super.create(entity);
        return Response.ok("OK", MediaType.TEXT_PLAIN).build();
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Rastidesc entity) {
        super.edit(entity);

    }

    @DELETE
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})

    @Override
    public Response remove(Rastidesc entity1) {

        try {
            em.getTransaction().begin();
            Rastidesc entity = getEntityManager().getReference(Rastidesc.class, entity1.getId());
            getEntityManager().remove(entity);

            em.getTransaction().commit();
            return Response.ok("OK", MediaType.TEXT_PLAIN).build();

        } catch (Exception e) {
            return Response.serverError().build();
        }

    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Rastidesc find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public List<Rastidesc> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Rastidesc> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
